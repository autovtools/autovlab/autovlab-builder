#!/bin/bash
set -e
export REGISTRY="autovlab/"

if [ ! -d autovlab-dev ]; then
    echo "Run from directory containing autovlab-dev/"
    exit 1
fi
pushd autovlab-dev
    docker build . --target pip --tag "${REGISTRY}autovlab:pip"
popd

PROJECTS="terrashell terradactsl packinator autovlab"
STAGES="builder base debug"
for project in ${PROJECTS}; do
    for stage in ${STAGES}; do
        pushd "${project}"
        tag="${REGISTRY}${project}:${stage}"

        cat << EOF
###############################################################################
${tag} START
###############################################################################
EOF
        docker build . --target "${stage}" --tag "${tag}"

        cat << EOF
###############################################################################
${tag} END
###############################################################################
EOF
        popd
    done
done

# Retag base -> latest since we had a sucessful build
for project in ${PROJECTS}; do
    docker tag "${REGISTRY}${project}:base" "${REGISTRY}${project}:latest"
done
