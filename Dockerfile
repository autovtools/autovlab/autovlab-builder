FROM python:slim as pip

RUN useradd -ms /bin/bash autovlab

RUN apt update && apt install -y --no-install-recommends \
    git \
    gcc \
    python3-dev \
    && \
    python -m pip install --user --no-warn-script-location --no-cache-dir --upgrade pip && \
    rm -rf /var/lib/apt/lists/*

